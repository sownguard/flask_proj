import psycopg2 
import logging
import os
from initdb import Init_DB, Create_DB



logging_level = os.environ.get('LOG_LEVEL')
database_host = os.environ.get('DB_HOST')

class Initialize():

	def __init__(self):

		### Setting up the logging config
		if logging_level:
			if logging_level.upper() == 'DEBUG':
				logging.basicConfig(filename='initdb.log', level=logging.DEBUG,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'INFO':
				logging.basicConfig(filename='initdb.log', level=logging.INFO,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'ERROR':
				logging.basicConfig(filename='initdb.log', level=logging.ERROR,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'WARNING':
				logging.basicConfig(filename='initdb.log', level=logging.WARNING,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'CRITICAL':
				logging.basicConfig(filename='initdb.log', level=logging.CRITICAL,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
		else:
			logging.basicConfig(filename='initdb.log', level=logging.ERROR,
		 		format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


		### If env. vars are empty - the error occurs
		###
		if (type(logging_level)!=str) or (type(database_host)!=str):
			self.show_message('---------Please, set env. vars LOG_LEVEL and DB_HOST----------', 'error') 
			pass
		else:
			Create_DB()
			Init_DB()

	### The function that can prints 
	### the message and puts it into the log file
	def show_message(self, message, level=logging_level):
		message = str(message)
		if level.upper() == 'INFO':
			logging.info(message)
		elif level.upper() == 'ERROR':
			logging.error(message)
			print(level.upper() + ' :: ' + message)
		elif level.upper() == 'WARNING':
			logging.warrning(message)
			print(level.upper() + ' :: ' + message)
		elif level.upper() == 'DEBUG':
			logging.debug(message)
			print(level.upper() + ' :: ' + message)
		elif level.upper() == 'CRITICAL':
			logging.critical(message)
			print(level.upper() + ' :: ' + message)





