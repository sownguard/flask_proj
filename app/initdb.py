import psycopg2 
import logging
import os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


### Checking whether env. vars are set



logging_level = os.environ.get('LOG_LEVEL')
database_host = os.environ.get('DB_HOST')

def show_message(message, level=logging_level):
	message = str(message)
	if level.upper() == 'INFO':
		logging.info(message)
	elif level.upper() == 'ERROR':
		logging.error(message)
		print(level.upper() + ' :: ' + message)
	elif level.upper() == 'WARNING':
		logging.warrning(message)
		print(level.upper() + ' :: ' + message)
	elif level.upper() == 'DEBUG':
		logging.debug(message)
		print(level.upper() + ' :: ' + message)
	elif level.upper() == 'CRITICAL':
		logging.critical(message)
		print(level.upper() + ' :: ' + message)


### Truncate the log file 
with open('initdb.log', 'w'):
	pass

class Create_DB():
	def __init__(self):
		try:
			connect_str = ("user='postgres' host='{db_host}' " + \
		                      "password=''").format(db_host=database_host)

			show_message('---------Connecting to the database----------', 'info')
			conn = psycopg2.connect(connect_str)
			conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
			cursor = conn.cursor()
			cursor.execute('''CREATE USER flask_proj;''')
			cursor.execute('''CREATE DATABASE flask_proj;''')
			cursor.execute('''GRANT ALL PRIVELEGES ON DATABASE flask_proj to flask_proj;''')
		except Exception as sql_exc:

		#	22 9-45
			print(sql_exc)


#### HOW TO RESTART PSQL WITH NEW PG_HBA!!!!!!!!!!!!!!
class Init_DB():
	def __init__(self):	
		try:
		    connect_str = ("user='flask_proj' host='{db_host}' " + \
		                      "password=''").format(db_host=database_host)
		    show_message('---------Connecting to the database----------', 'info')
		    conn = psycopg2.connect(connect_str)
		    cursor = conn.cursor()
		    cursor.execute("""DROP TABLE IF EXISTS tutorials;""")
		    cursor.execute("""CREATE TABLE tutorials (name char(40));""")
		    show_message('---------Creating the test table----------', 'info')
		    cursor.execute("""INSERT INTO tutorials SELECT (random()::text) from generate_Series(1,5);""")
		    show_message('---------Inserting test values----------', 'info')
		    cursor.execute("""SELECT * from tutorials""")
		    show_message('---------Getting test values----------', 'info')
		    #conn.commit() # <--- makes sure the change is shown in the database
		    show_message('---------Commtitting the transaction----------', 'info')
		    rows = cursor.fetchall() ### <------- getting the output of the SELECT query
		    show_message('---------Counting rows----------', 'info')
		    try:
		    	cursor.execute("""DROP TABLE tutorials;""")
		    except Exception as e:
		    	raise e	
		    	cursor.execute("""DROP TABLE tutorials;""")
		    if len(rows) == 5:
		    	show_message('---------That\'s all OK----------', 'info')
		    	show_message('---------Deleting test table----------', 'info')
		    	show_message('---------Database initizalized successfully----------', 'info')
		    	print('---------Database initizalized successfully----------')
		    conn.commit()
		    cursor.close()
		    conn.close()
		except Exception as e:
			show_message('---------Something went wrong. Check credentials of connection to DB----------', 'error')
			print('\n')
			show_message(e, 'error')
