FROM python:3
ENV DB_HOST localhost
ENV LOG_LEVEL info
WORKDIR /app
ADD . /app
RUN pip install -r requirements.txt

CMD gunicorn --chdir /app/app --bind :5000 app:app
